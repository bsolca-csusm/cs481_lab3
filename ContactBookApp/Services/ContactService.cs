﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using ContactBookApp.Models;

namespace ContactBookApp.Services
{
    class ContactService
    {
        private ObservableCollection<Contact> _contacts = new ObservableCollection<Contact>
        {
            new Contact
            {
                Id = 0,
                Email = "john.doe@example.com",
                FirstName = "John",
                IsBlocked = false,
                LastName = "Doe",
                Phone = "0651496613"
            },
            new Contact
            {
                Id = 1,
                Email = "alice@example.com",
                FirstName = "Alice",
                IsBlocked = false,
                LastName = "Mice",
                Phone = "0602020202"
            }
        };

        public void AddContact(Contact newContact)
        {
            _contacts.Add(newContact);
        }
        public ObservableCollection<Contact> GetContacts() => _contacts;
        public Contact GetContactById(int id) => _contacts.Single(e => e.Id == id);
        public void EditContact(Contact editedContact)
        {
            var matchingContact = _contacts.FirstOrDefault(contact => contact.Id == editedContact.Id);
            if (matchingContact != null)
            {
                matchingContact = editedContact;
            }
        }

        public void DeleteContact(int id)
        {
            var itemToRemove = _contacts.Single(e => e.Id == id);
            _contacts.Remove(itemToRemove);
        }
    }
}